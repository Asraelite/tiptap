export default {
	chat: {
		message: 'string',
	},
	pixel: {
		color: 'uint8',
		x: 'uint16',
		y: 'uint16',
	},
	progress: {
		current: 'uint32',
		next: 'uint32',
	},
	cooldown: {
		time: 'uint32',
	},
	canvas: {
		width: 'uint16',
		height: 'uint16',
		data: 'buffer',
	},
	serverPixel: {
		color: [ 'uint8' ],
		x: 'uint16',
		y: 'uint16',
	},
	palette: {
		colors: [ [ 'uint8' ] ],
	}
};
