import schemapack from 'schemapack';
import schemaData from '../common/schemas.mjs';

let schemas = {};
for (let id in schemaData) {
	schemas[id] = schemapack.build({
		__schemaId: 'uint8',
		...schemaData[id]
	});
}

const schemaNameIndex = Object.keys(schemas);

export function serialize(type, object) {
	return schemas[type].encode({
		__schemaId: nameToId(type),
		...object
	});
}

export function deserialize(object) {
	try {
		let id = new Uint8Array(object)[0];
		let type = idToName(id);
		return [type, schemas[type].decode(object)];
	} catch(e) {
		return ['error', e];
	}
}

function nameToId(schemaName) {
	return schemaNameIndex.indexOf(schemaName);
}

function idToName(schemaId) {
	return schemaNameIndex[schemaId];
}
