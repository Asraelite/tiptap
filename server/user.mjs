import crypto from 'crypto';

import db from './db.mjs';
import config from './config.mjs';
import Palette from './palette.mjs';
import * as board from './board.mjs';

let cooldowns = new Map();

export default class User {
	constructor(connection) {
		this.connection = connection;
		this.admin = false;
		this.palette = new Palette();
		this.cooldown = config.COOLDOWN;
		this.id = crypto.createHash('md5').update(connection.ip).digest("hex");

		db.push(`/${this.id}`, {}, false); // Create entry if doesn't exist.

		if (!cooldowns.has(this.id)) {
			cooldowns.set(this.id, 0);
		}

		({
			pixelsPlaced: this.pixelsPlaced = 0
		} = db.getData(`/${this.id}`));

		this.level = this.palette.update(this.pixelsPlaced);
	}

	init() {
		this.connection.send('canvas', board.getAll());
		this.sendPalette();
		this.sendProgress();
	}

	sendPalette() {
		this.connection.send('palette', {
			colors: this.palette.colors,
		});
	}

	sendProgress() {
		this.connection.send('progress', {
			current: this.pixelsPlaced,
			next: this.palette.levelToCount(this.level),
		});
	}

	placePixel(x, y, colorId) {
		let color = this.palette.getColor(colorId);
		if (!this.canPlace()) return;
		if (!color || color.length != 4) return;

		let cooldown = cooldowns.get(this.id) + config.PIXEL_COOLDOWN;
		let max = config.PIXEL_COOLDOWN * config.COOLDOWN_GROUPING;
		if (Date.now() - cooldown >= 0) {
			cooldown = Date.now() + config.PIXEL_COOLDOWN;
		}
		cooldowns.set(this.id, cooldown);

		board.setPixel(x, y, color);
		this.connection.send('cooldown', {
			time: cooldown - Date.now(),
		});

		this.pixelsPlaced++;
		let pinked = this.palette.pinked;
		let level = this.palette.update(this.pixelsPlaced);

		if (level != this.level || pinked != this.palette.pinked) {
			this.level = level;
			this.sendPalette();
		}

		this.sendProgress();
	}

	disconnect() {
		db.push(`/${this.id}`, {
			pixelsPlaced: this.pixelsPlaced,
			ip: this.connection.ip,
		}, false);
	}

	canPlace() {
		let cooldown = cooldowns.get(this.id) + config.PIXEL_COOLDOWN;
		let max = config.PIXEL_COOLDOWN * config.COOLDOWN_GROUPING;
		return cooldown < Date.now() + max;
	}

	sendChat(message) {
		console.log('chat:', message);
	}
}
