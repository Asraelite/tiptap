import * as net from './net.mjs';
import * as board from './board.mjs';

board.init();
net.listen(8192);
