import uws from 'uws';

import * as serialize from './serialize.mjs';
import config from './config.mjs';
import User from './user.mjs';

let wss;
const connections = new Set();

class Connection {
	constructor(ws) {
		this.ip = ws._socket.remoteAddress;
		this.socket = ws;
		this.user = new User(this);

		this.user.init();
	}

	message(msg) {
		let [type, data] = serialize.deserialize(msg);
		if (type === 'pixel') {
			this.user.placePixel(data.x, data.y, data.color);
		} else if (type === 'chat') {
			this.user.sendChat(data.message);
		} else {
			console.log(`Invalid packet from ${this.ip}: ${type}`);
		}
	}

	send(type, data) {
		this.socket.send(serialize.serialize(type, data));
	}

	close() {
		this.user.disconnect();
	}
}

export function closeAll() {
	connections.forEach(c => c.close());
}

export function listen(port) {
	wss = new uws.Server({ port: port });

	wss.on('connection', onConnection);
}

export function broadcast(type, data) {
	connections.forEach(c => c.send(type, data));
}

function onConnection(ws) {
	let connection = new Connection(ws);
	connections.add(connection);
	ws.on('message', (msg) => connection.message(msg));
	ws.on('close', () => {
		connection.close();
		connections.delete(connection);
	});
}
