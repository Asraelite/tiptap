import config from './config.mjs';
import color from './color.mjs';

export default class Palette {
	constructor() {
		this.colors = [];
		this.pinked = false;
	}

	update(pixelsPlaced) {
		let level = this.countToLevel(pixelsPlaced);
		level = Math.floor(level) + 2;

		if (Math.random() < config.PINK_CHANCE && !this.pinked) {
			this.colors = Array(20).fill().map(() => {
				let shade = Math.random() * 150 + 50;
				return [255, 255 - shade | 0, 255 - (shade / 2) | 0, 255];
			});
			this.pinked = true;
			console.log('pinked');
		} else if(!this.pinked) {
			this.colors = Object.values(color).splice(0, level);
		}

		return level;
	}

	countToLevel(count) {
		return Math.log(1 + (count / config.LEVEL_DIVIDEND))
			/ Math.log(config.LEVEL_EXPONENT);
	}

	levelToCount(level) {
		return Math.ceil(config.LEVEL_DIVIDEND
			* ((config.LEVEL_EXPONENT ** (level - 1)) - 1));
	}

	randomColor() {
		return Array(3).fill().map(c => Math.random() * 255 | 0).concat(255);
	}

	getColor(id) {
		if (id < 0 || id > this.colors.length) return color.WHITE;
		return this.colors[id];
	}
}
