import fs from 'fs';
import util from 'util';
import sharp from 'sharp';

import db from './db.mjs';
import color from './color.mjs';
import * as net from './net.mjs';
import config from './config.mjs';

const users = new Set();

let buffer = new Uint8Array(0);
let width = 0;
let height = 0;
let pixelsPlaced = 0;

export function init() {
	db.push('/pixels', {}, false);

	({
		pixelsPlaced = 0
	} = db.getData('/pixels'));

	resize(2, 2);

	try {
		load();
	} catch(e) {
	}

	save();

	setInterval(save, config.SAVE_INTERVAL);

	process.on('SIGINT', async () => {
		try {
			await save();
			net.closeAll();
		} catch(err) {
			console.log(err);
		} finally {
        	process.exit();
		}
	});
}

export function addUser(user) {
	users.add(user);
}

function grow() {
	resize(width + 2, height + 2);
}

export function resize(newWidth, newHeight, fillColor = color.WHITE) {
	let widthDif = (newWidth - width) / 2;
	let heightDif = (newHeight - height) / 2;

	let newBuffer = new Uint8Array(newWidth * newHeight * 4);

	for (let ny = 0; ny < newHeight; ny++) {
		for (let nx = 0; nx < newWidth; nx++) {
			let [ox, oy] = [-widthDif + nx, -heightDif + ny];
			let ni = (ny * newWidth + nx) * 4;

			if (oy < 0 || ox < 0 || oy >= height || ox >= width) {
				newBuffer.set(fillColor, ni);
			} else {
				let oi = (oy * width + ox) * 4;
				newBuffer.set(buffer.slice(oi, oi + 4), ni);
			}
		}
	}

	width = newWidth;
	height = newHeight;
	buffer = newBuffer;

	broadcast();
}

function broadcast() {
	net.broadcast('canvas', getAll());
}

export function getAll() {
	return {
		width,
		height,
		data: Buffer.from(buffer)
	};
}

export function setPixel(x, y, color) {
	if (x < 0 || y < 0 || x >= width || y >= height) return;
	let i = (y * width + x) * 4;
	buffer.set(color, i);
	net.broadcast('serverPixel', { x, y, color });

	pixelsPlaced++;
	if (width * height <= pixelsPlaced * config.GROWTH_SPEED) {
		grow();
	}
}

function load() {
	if (!fs.existsSync(config.RAW_FILE)) return;
	let data = fs.readFileSync(config.RAW_FILE);
	width = Math.ceil(Math.sqrt(data.length / 4));
	height = width;
	resize(width, height);
	buffer.set(data);
}

async function save() {
	let writeFile = util.promisify(fs.writeFile);
	db.push('/pixels', { pixelsPlaced }, false);

	await writeFile(config.RAW_FILE, buffer, 'binary');

	await sharp(Buffer.from(buffer), {
		raw: {
			width,
			height,
			channels: 4
		}
	}).resize(32).toFile('dist/favicon.png');
}
