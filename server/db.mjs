import JsonDb from 'node-json-db';

import config from './config.mjs';

export default new JsonDb(config.DB_FILE);
