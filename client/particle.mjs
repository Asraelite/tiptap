export default class Particle {
	constructor(type, x, y, color) {
		this.type = type;
		if (this.type == 'dot') {
			this.size = Math.random() * 0.2 + 0.02;
			this.xvel = (Math.random() - 0.5) * 0.2;
			this.yvel = (Math.random() - 0.8) * 0.2;
			this.x = x + Math.random();
			this.y = y + Math.random();
			this.life = Math.random() * 40 + 10;
			this.opacity = 1;
		} else {
			this.size = 1;
			this.xvel = 0;
			this.yvel = 0;
			this.x = x;
			this.y = y;
			this.life = 24;
			this.opacity = 1;
		}
		this.color = `rgb(${color.join(',')})`;
	}

	tick() {
		this.x += this.xvel;
		this.y += this.yvel
		this.life--;

		if (this.type === 'grow') {
			this.opacity -= 0.04;
			this.size += 0.02;
			this.x -= 0.01;
			this.y -= 0.01;
		} else {
			this.yvel += 0.01;
		}
	}
}
