import * as graphics from './graphics.mjs';
import * as net from './net.mjs';

export let currentColor = 0;
let container;
export let pixelsPlaced = 0;
let nextColor = 0;
let colors = [];

export function currentRgb() {
	if (currentColor >= colors.length) {
		return '#fff';
	}
	return getRgb(currentColor);
}

export function placePixel(x, y) {
	net.send('pixel', { x, y, color: currentColor });
	pixelsPlaced++;
}

export function updateProgress(data) {
	nextColor = data.next;
	pixelsPlaced = data.current;

	document.querySelector('#progress').innerHTML =
		`Next color: ${nextColor - pixelsPlaced}`;
}

export function currentColorArr() {
	return colors[currentColor];
}

export function getRgb(id) {
	return `rgb(${colors[id].join(',')})`;
}

export function init() {
	container = document.querySelector("#colors");
}

export function loadPalette(data) {
	colors = data.colors;
	container.innerHTML = '';
	Array.from(colors.entries()).sort(([ai, ac], [bi, bc]) => {
		// Uncomment for hue sorting.
		//return getHue(...ac) - getHue(...bc);
		return ai - bi;
	}).forEach(([i, c]) => createButton(i));
}

function setColor(id) {
	document.querySelector(`#color-${currentColor}`)
		.classList.remove('selected');

	currentColor = id;

	document.querySelector(`#color-${currentColor}`)
		.classList.add('selected');
}

export function findAndSetColor([r, g, b]) {
	let i = colors.findIndex(([cr, cg, cb]) => r == cr && g == cg && b == cb);
	if (i === -1) return false;
	setColor(i);
	return true;
}

function getHue(r, g, b) {
	[r, g, b] = [r / 255, g / 255, b / 255];
	let max = Math.max(r, g, b);
	let min = Math.min(r, g, b);

	if (max === r) {
		return (g - b) / (max - min);
	} else if (max === b) {
		return 2 + (b - r) / (max - min);
	} else {
		return 4 + (r - g) / (max - min);
	}
}

function createButton(id) {
	let div = document.createElement('div');
	div.id = `color-${id}`;
	div.classList.add('color');
	div.style.backgroundColor = getRgb(id);
	if (id === currentColor)
		div.classList.add('selected');
	div.addEventListener('click', () => {
		setColor(id);
	});

	container.appendChild(div);
}
