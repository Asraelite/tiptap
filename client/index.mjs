import * as graphics from './graphics.mjs';
import * as net from './net.mjs';
import * as input from './input.mjs';
import * as palette from './palette.mjs';

window.addEventListener('load', init);

async function init() {
	graphics.init();
	palette.init();
	input.init();
	await net.connect();
	graphics.start();
}
