import * as serialize from './serialize.mjs';
import * as graphics from './graphics.mjs';
import * as palette from './palette.mjs';
import * as input from './input.mjs';

const WS_PORT = 8192;

let ws;

export async function connect() {
	ws = new WebSocket(`ws://${location.hostname}:${WS_PORT}`);
	ws.binaryType = "arraybuffer";
	ws.addEventListener('message', msg => message(msg));
	ws.addEventListener('close', () => setTimeout(() => {
		location.reload()
	}, 5000));
	return new Promise(res => ws.addEventListener('open', res));
}

export function send(type, data) {
	let buffer = serialize.serialize(type, data);
	ws.send(buffer);
}

function message(msg) {
	let [type, data] = serialize.deserialize(msg.data);
	if (type === 'canvas') {
		graphics.loadCanvas(data);
	} else if (type === 'serverPixel') {
		graphics.placePixel(data);
	} else if (type === 'palette') {
		palette.loadPalette(data);
	} else if (type === 'cooldown') {
		input.setCooldown(data);
	} else if (type === 'progress') {
		palette.updateProgress(data);
	}
}

export function test() {

}
