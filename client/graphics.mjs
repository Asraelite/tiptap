import * as input from './input.mjs';
import * as palette from './palette.mjs';
import Particle from './particle.mjs';

export let canvas, context;
let tempCanvas, tempContext;

let renderQueued = false;
let tempReady = false;
let particles = new Set();

const START_ZOOM = 5;
const ZOOM_FACTOR = 1.7;

let view = {
	x: 0,
	y: 0,
	zoom: START_ZOOM,

	interpolation: {
		from: {
			x: 0,
			y: 0,
			zoom: START_ZOOM,
		},
		to: {
			x: 0,
			y: 0,
			zoom: START_ZOOM,
		},
		progress: 0,
		speed: 0.3,
	}
};

export function init() {
	canvas = document.querySelector('#viewport');
	context = canvas.getContext('2d');
	tempCanvas = document.querySelector('#temp');
	tempContext = tempCanvas.getContext('2d');
	tempCanvas.width = 0;
	tempCanvas.height = 0;
}

export function loadCanvas(packet) {
	let [oldWidth, oldHeight] = [tempCanvas.width, tempCanvas.height];
	tempCanvas.width = packet.width;
	tempCanvas.height = packet.height;
	let imageData = new ImageData(new Uint8ClampedArray(packet.data),
		packet.width, packet.height);
	tempContext.putImageData(imageData, 0, 0);

	moveView((tempCanvas.width - oldWidth) / 2 * view.zoom,
		(tempCanvas.height - oldHeight) / 2 * view.zoom);

	queueRender();
}

export function withinCanvas(x, y) {
	return x >= 0 && x < tempCanvas.width
		&& y >= 0 && y < tempCanvas.height;
}

export function placePixel(data) {
	let imageData = new ImageData(new Uint8ClampedArray(data.color), 1, 1);
	tempContext.putImageData(imageData, data.x, data.y);
	for (let i = 0; i < 10; i++) {
		particles.add(new Particle('dot', data.x, data.y, data.color));
	}
	queueRender();
}

export function colorPick(x, y, color) {
	particles.add(new Particle('grow', x, y, color));
	queueRender();
}

function transform() {
	context.translate(canvas.width / 2, canvas.height / 2);
	context.scale(view.zoom, view.zoom);
	context.translate(-view.x, -view.y);
}

export function render() {
	canvas.width = canvas.clientWidth;
	canvas.height = canvas.clientHeight;
	context.imageSmoothingEnabled = false;

	context.clearRect(0, 0, canvas.width, canvas.height);

	context.save();
	transform();
	context.fillStyle = '#eee';
	context.fillRect(-0.1, -0.1,
		tempCanvas.width + 0.2, tempCanvas.height + 0.2);
	context.drawImage(tempCanvas, 0, 0);

	let cooldown = input.getCooldownStatus();
	let [mx, my] = input.mousePos;

	if (withinCanvas(mx, my)) {
		context.fillStyle = palette.currentRgb();
		context.fillRect(mx | 0, my | 0, 1, 1);
	}

	context.restore();

	if (!cooldown.done) {
		context.globalAlpha = 0.7;
		context.fillStyle = '#ccc';
		let sx = canvas.width / 2 - 70;
		let sy = canvas.height - 100;
		context.fillRect(sx, sy, 140, 30);
		context.globalAlpha = 1;
		context.fillStyle = '#42b3f4';
		context.fillRect(sx, sy, 140 * (1 - cooldown.progress), 30);
	}

	context.save();
	transform();

	particles.forEach(p => {
		p.tick();
		context.fillStyle = p.color;
		context.globalAlpha = p.opacity;
		context.fillRect(p.x, p.y, p.size, p.size);
		if (p.life <= 0) {
			particles.delete(p);
		}
		queueRender();
	});

	context.restore();

	interpolate();

	if(!cooldown.done) {
		queueRender();
	}
}

function interpolate() {
	let int = view.interpolation;

	int.progress += (1 - int.progress) * int.speed;

	let ft = int.progress;
	let ff = (1 - int.progress);
	let zoom = int.to.zoom * ft + int.from.zoom * ff;
	let x = int.to.x * ft + int.from.x * ff;
	let y = int.to.y * ft + int.from.y * ff;

	view.zoom = zoom;
	view.x = x;
	view.y = y;

	if (int.progress > 0.99) {
		int.progress = 1;
	} else {
		queueRender();
	}
}

export function changeZoom(direction) {
	let delta = ZOOM_FACTOR;
	if (direction > 0) delta = 1 / delta;

	let newZoom = view.zoom * delta;

	newZoom = Math.max(1, Math.min(80, newZoom));

	let [x, y] = input.mousePos;
	let [dx, dy] = [x - view.x, y - view.y];

	let hw = canvas.width / 2;
	let hh = canvas.height / 2;
	let moveScale = view.zoom / newZoom;
	view.interpolation.from.x = view.x;
	view.interpolation.from.y = view.y;
	//view.interpolation.to.x += dx * moveScale;
	//view.interpolation.to.y += dy * moveScale;

	// TODO: Fix zooming

	view.interpolation.progress = 0;
	view.interpolation.from.zoom = view.zoom;
	view.interpolation.to.zoom = newZoom;

	queueRender();
}

export function screenToTile(sx, sy) {
	let x = (sx - canvas.width / 2) / view.zoom + view.x;
	let y = (sy - canvas.height / 2) / view.zoom + view.y;
	return [x, y];
}

export function getColorAt(x, y) {
	return tempContext.getImageData(x, y, 1, 1).data;
}

export function moveView(x, y) {
	let tx = view.interpolation.to.x + x / view.zoom;
	tx = Math.max(Math.min(tx, tempCanvas.width), 0);
	let ty = view.interpolation.to.y + y / view.zoom;
	ty = Math.max(Math.min(ty, tempCanvas.height), 0);
	view.interpolation.to.x = tx;
	view.interpolation.to.y = ty;
}

function setView(x, y) {
	view.interpolation.to.x = x;
	view.interpolation.from.x = x;
	view.interpolation.to.y = y;
	view.interpolation.from.y = y;
}

export function startMove() {
	canvas.style.cursor = 'move';
}

export function endMove() {
	canvas.style.cursor = 'default';
	queueRender();
}

export function start() {
	if (renderQueued) {
		renderQueued = false;
		render();
	}

	requestAnimationFrame(start);
}

export function queueRender() {
	renderQueued = true;
}
