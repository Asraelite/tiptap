import * as graphics from './graphics.mjs';
import * as net from './net.mjs';
import * as palette from './palette.mjs';

export let mousePos = [0, 0];
export let cooldownEnd = 0;
export let cooldownStart = 0;
let dragStart = [0, 0];
let dragged = false;
let screenPos = [0, 0];
let mouseButtons = {};

export function init() {
	window.addEventListener('mousemove', event => {
		let oldPos = screenPos;
		screenPos = [event.pageX, event.pageY];
		mousePos = graphics.screenToTile(event.pageX, event.pageY);

		if (mouseButtons[0]) {
			let [dx, dy] = [screenPos[0] - oldPos[0], screenPos[1] - oldPos[1]];
			let [dsx, dsy] = [screenPos[0] - dragStart[0],
				screenPos[1] - dragStart[1]];

			if ((dragged && (dx != 0 || dy != 0)) ||
				(!dragged && (Math.abs(dsx) > 15 || Math.abs(dsy) > 15))) {
				graphics.moveView(-dx, -dy);
				graphics.startMove();
				dragged = true;
			}
		}

		graphics.queueRender();
	});

	graphics.canvas.addEventListener('wheel', event => {
		graphics.changeZoom(event.deltaY > 0 ? 1 : -1);
	}, {passive: false});

	window.addEventListener('resize', () => {
		graphics.queueRender();
	});

	window.addEventListener('mousedown', event => {
		event.preventDefault();
	});

	graphics.canvas.addEventListener('mousedown', event => {
		dragStart = [event.offsetX, event.offsetY];
		dragged = false;
		mouseButtons[event.button] = true;
	});

	window.addEventListener('mouseup', event => {
		mouseButtons[event.button] = false;
		graphics.endMove();
	});

	graphics.canvas.addEventListener('mouseup', event => {
		mouseButtons[event.button] = false;
		graphics.endMove();

		if (dragged) return;

		let [x, y] = graphics.screenToTile(event.offsetX, event.offsetY);
		x |= 0;
		y |= 0;
		if (!graphics.withinCanvas(x, y)) return;

		if (event.button === 0) {
			palette.placePixel(x, y);
		} else if (event.button === 1) {
			let res = palette.findAndSetColor(graphics.getColorAt(x, y));
			if (res) {
				graphics.colorPick(x, y, palette.currentColorArr());
			}
		}
	});
}

export function getCooldownStatus() {
	let now = Date.now();
	let done = now >= cooldownEnd;
	return {
		done: done,
		progress: (cooldownEnd - Date.now()) / 60000,
	};
}

export function setCooldown(data) {
	cooldownStart = Date.now();
	cooldownEnd = Date.now() + data.time;
}
